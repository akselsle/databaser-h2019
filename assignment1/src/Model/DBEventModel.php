<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing
  * data in a MySQL database using PDO.
  * @author Rune Hjelsvold
  * @see http://php-html.net/tutorials/model-view-controller-in-php/
  *      The tutorial code used as basis.
  */

require_once("AbstractEventModel.php");
require_once("Event.php");
require_once("dbParam.php");

/** The Model is the class holding data about a archive of events.
  * @todo implement class functionality.
  */


class DBEventModel extends AbstractEventModel
{
    protected $db = null;

    /**
      * @param PDO $db PDO object for the database; a new one will be created if
      *                no PDO object is passed
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function __construct($db = null)
    {
        $dsn = 'mysql:dbname='.DB_NAME.';host=127.0.0.1';
        if ($db) {
            $this->db = $db;
        } else {
          try{
            $this->db = new PDO($dsn, DB_USER, DB_PWD);  //Kobler til DB
          }catch(PDOException $e){  //sender med error-message hvis det ikke funket
            $error_message = $e->getMessage();
            echo $error_message;
            exit();
          }
        }
    }

    /** Function returning the complete list of events in the archive. Events
      * are returned in order of id.
      * @return Event[] An array of event objects indexed and ordered by id.
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function getEventArchive()
    {
      $eventList = array();
      $sql = 'SELECT title, date, description, id   FROM event';

      try{
      $stmt = $this->db->query($sql);
      $rows = $stmt->fetchAll();
      foreach ($rows as $row){  //henter en og en row
        $newEvent = new Event(  //oppretter et event
          $row['title'],
          $row['date'],
          $row['description'],
          $row['id']
        );
        $eventList[] = $newEvent;
      }
    }catch(Exception $e){
      $error_message = $e->getMessage();
      echo $error_message;
      exit();
    }
        return $eventList;  //retunerer eventListen
    }

    /** Function retrieving information about a given event in the archive.
      * @param integer $id the id of the event to be retrieved
      * @return Event|null The event matching the $id exists in the archive;
      *         null otherwise.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      */
    public function getEventById($id)
    {
        $event = null;
        try{
        Event::verifyId($id);
        $stmt = $this->db->prepare("SELECT id, title, date, description FROM event WHERE id=:id");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $res = $stmt->fetch(PDO::FETCH_ASSOC);
        $event = new Event(
          $res['title'],
          $res['date'],
          $res['description'],
          $res['id']
        );

      } catch(PDOException $e){
          $error_message = $e->getMessage();
          echo $error_message;
          exit();
        }

        return $event;
    }

    /** Adds a new event to the archive.
      * @param Event $event The event to be added - the id of the event will be set after successful insertion.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
      */
    public function addEvent($event)
    {


        try{
        $event->verify();

        $stmt = $this->db->prepare('INSERT INTO event(title,date,description) '.' VALUES(:title, :date, :description)');
        $stmt->bindValue(':title', $event->title);
        $stmt->bindValue(':date', $event->date);
        $stmt->bindValue(':description', $event->description);
        $stmt->execute();
        $event->id = $this->db->lastInsertId();
      }catch(PDOException $e){
          $error_message = $e->getMessage();
          echo $error_message;
          exit();
        }

    }

    /** Modifies data related to a event in the archive.
      * @param Event $event The event data to be kept.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
     */
    public function modifyEvent($event)
    {
      try{
      //validate user input
      $event->verify();

      $stmt = $this->db->prepare('UPDATE event SET title=:title, date=:date, description=:description WHERE id=:id');
      $stmt->bindValue(':title', $event->title);
      $stmt->bindValue(':date', $event->date);
      $stmt->bindValue(':description', $event->description);
      $stmt->bindValue(':id',$event->id);
      //updates the DB
      $affectedNo = $stmt->execute();
    }catch(PDOException $e){
      $error_message = $e->getMessage();
      echo $error_message;
      exit();
    }
      return $affectedNo;
    }

    /** Deletes data related to a event from the archive.
      * @param $id integer The id of the event that should be removed from the archive.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
     */
    public function deleteEvent($id)
    {
        try{
        //validating input
        Event::verifyId($id);
          $stmt = $this->db->prepare("DELETE FROM event WHERE id=:id");
          $stmt->bindValue(':id', $id);
          $stmt->execute();
        }catch(PDOException $e){
          $error_message = $e->getMessage();
          echo $error_message;
          exit();
        }
    }
}
